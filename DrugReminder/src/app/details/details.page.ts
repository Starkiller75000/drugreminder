import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Drug} from '../models/Drug';
import {DrugService} from '../services/drug.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

const id = 0;

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  drug: Drug;

  constructor(private activatedRoute: ActivatedRoute, public drugService: DrugService, private localNotifications: LocalNotifications) { }

  ngOnInit() {
    const name = this.activatedRoute.snapshot.paramMap.get('name');
    this.drugService.getDrugByName(name).subscribe((res: Drug) => {
      this.drug = res;
    });
  }

  onClick() {
    this.localNotifications.schedule([
      {
        id: id + 1,
        title: 'DrugReminder',
        text: `Don't forget to take you ${this.drug.name}`,
        trigger: {at: new Date(new Date().getTime() + 7200)},
        led: 'FF0000',
        foreground: true,
      }
    ]);
  }

}
