import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {Drug} from '../models/Drug';

@Injectable({
  providedIn: 'root'
})
export class DrugService {

  constructor(private http: HttpClient) { }

  getDrugs(): Observable<Drug[]> {
    return this.http.get<Drug[]>('https://testforgithu.herokuapp.com/drugs');
  }

  getDrugByName(name: string): Observable<Drug> {
    return this.http.get<Drug>(`https://testforgithu.herokuapp.com/drug/${name}`);
  }
}
