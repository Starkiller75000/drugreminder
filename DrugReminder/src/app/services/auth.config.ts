// src/app/services/auth.config.ts

export const AUTH_CONFIG = {
    // Needed for Auth0 (capitalization: ID):
    clientID: 'kPNVMPXHIYXyrk0OnVCKanLdHZSNGjcE',
    // Needed for Auth0Cordova (capitalization: Id):
    clientId: 'kPNVMPXHIYXyrk0OnVCKanLdHZSNGjcE',
    domain: 'dev-30nz9uiw.eu.auth0.com',
    packageIdentifier: 'drugreminder.com' // config.xml widget ID, e.g., com.auth0.ionic
};
