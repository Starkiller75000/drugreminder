import { Component, OnInit } from '@angular/core';
import {DrugService} from '../../services/drug.service';
import {Drug} from '../../models/Drug';

@Component({
  selector: 'app-drug-list',
  templateUrl: './drug-list.component.html',
  styleUrls: ['./drug-list.component.scss'],
})
export class DrugListComponent implements OnInit {

  loading = false;
  drugs: Drug[];

  constructor(public drugService: DrugService) {
    this.getDrugs();
  }

  ngOnInit() {}

  getDrugs() {
    this.loading = true;
    this.drugService.getDrugs().subscribe((res: Drug[]) => {
      this.drugs = res;
    });
    this.loading = false;
  }

}
