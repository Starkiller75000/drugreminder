import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DrugListComponent} from './drug-list/drug-list.component';
import {IonicModule} from '@ionic/angular';



@NgModule({
  declarations: [DrugListComponent],
    imports: [
        CommonModule,
        IonicModule
    ],
  exports: [DrugListComponent]
})
export class ComponentsModule { }
