export interface Drug {
    delay: number;
    unite: string;
    dose: number;
    name: string;
    imageUrl: string;
}
