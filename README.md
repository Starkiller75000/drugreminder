# DrugReminder

Deploying to an Android device is a fairly straightforward process.
If you have a working Android development environment, you’re ready to go.

## Requirements

To realise this deployment you’ll need some requirements :

-   [Java JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
-   [Android Studio](https://developer.android.com/studio/index.html)
-   Updated Android SDK tools, platform and component dependencies. Available through Android Studio’s [SDK Manager](https://developer.android.com/studio/intro/update.html)
- [Ionic](https://ionicframework.com/docs/intro/cli) framework
- [Cordova](https://cordova.apache.org/docs/en/latest/guide/cli/index.html)
- [Gradle](https://gradle.org/install/)
> **Note:** make sure to follow this link to install every thing you need.


When you’ve cloned the application’s repository you’ll need to run two commands

`npm install or npm i`

To install node module, and

`ionic cordova prepare android`

To build all the assets

## Running Your App

To run your app, all you have to do is enable USB debugging and Developer Mode on your Android device, then run `ionic cordova run android --device` from the command line.

This will produce a debug build of your app, both in terms of Android and Ionic’s code

Enabling USB debugging and Developer Mode can vary between devices, but is easy to look up with a Google search. You can also check out [Enabling On-device Developer Options](https://developer.android.com/studio/run/device.html#developer-device-options) in the Android docs.

## Production Builds

To run or build your app for production, run
`ionic cordova run android `
or
`ionic cordova build android --prod  --release`
